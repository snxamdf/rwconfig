package main

import (
	"fmt"
	"gitee.com/snxamdf/rwconfig/propertie"
)

func main() {
	prop := propertie.Init("D:\\SWT\\gopath\\src\\wzh-pkg\\WZH\\resources\\application.properties")
	fmt.Println(prop.GetProperties("environment.client1"))
	prop.SetProperties("environment.client1", "a1国国1a1国国1a1国国1")
	fmt.Println(prop.GetProperties("environment.client1"))

	prop.SetProperties("mqttHost", "121212")
	fmt.Println(prop.GetProperties("mqttHost"))
	prop.SetProperties("mysqlIp", "33333")
	fmt.Println(prop.GetProperties("mysqlIp"))
}
